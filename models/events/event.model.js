const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const EventSchema = new Schema({
  titre:  { type: String, default: "" },
  nbrparticipantmax:  { type: Number, default: 0 },
  nbrparticipant:  { type: Number, default: 0 },
  prix:  { type: Number, default: 0 },
  datedebut:  { type: Date, default: Date.now },
  datefin:  { type: Date, default: Date.now },
  city:  { type: String, default: "" },
  adresse:  { type: String, default: "" },
  description:  { type: String, default: "" },
  image:  { type: String, default: "" },
  categorie:  { type: String, default: "" },
  userparticipants: [[String]],
},{timestamps: true});
EventSchema.plugin(mongoosePaginate);

let Event = mongoose.model('Event',EventSchema);

module.exports = Event;
