var express = require('express');
var router = express.Router();

// Middleware
const authMiddle = require('../../middleware/auth.middleware');
const rolesMiddle = require('../../middleware/roles.middleware');
const {uploadPost} = require('../../middleware/upload.middleware');

const postController = require('../../controllers/events/post.controller');

router.use(express.json());
router.use(express.urlencoded({extended: false}));



// ***************** Api Register for user *****************
router.post('/add/:id',uploadPost.single('image'),(req, res) => {
  postController.add(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});

// ***************** Api update for user *****************
router.put('/update/:id',uploadPost.single('image'), (req, res) => {
  postController.update(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});

// ***************** Api delete for user *****************
router.delete('/delete/:id', (req, res) => {
  postController.delete(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});


// ***************** Api get  by id  *****************
router.get('/get/:id', (req, res) => {
  postController.get(req.params.id).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// ***************** Api list users *****************
router.get('/list', (req, res) => {
  postController.getList().then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// ***************** Api list event posts *****************
router.get('/listpostsevent/:id', (req, res) => {
  postController.geteventposts(req.params.id).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// *****************j'aime/j'aime plus actions   *****************
router.put('/jaime/:iduser/:idpost', (req, res) => {
  postController.jaime(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
router.put('/jaimeplus/:iduser/:idpost', (req, res) => {
  postController.jaimeplus(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// ***************** liste j'aime/j'aime plus    *****************




module.exports = router;
