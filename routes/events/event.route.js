var express = require('express');
var router = express.Router();

// Middleware
const authMiddle = require('../../middleware/auth.middleware');
const rolesMiddle = require('../../middleware/roles.middleware');
const {uploadEvent} = require('../../middleware/upload.middleware');

const eventsController = require('../../controllers/events/event.controller');

router.use(express.json());
router.use(express.urlencoded({extended: false}));



// ***************** Api Register for user *****************
router.post('/add',uploadEvent.single('image'),(req, res) => {
  eventsController.add(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});

// ***************** Api update for user *****************
router.put('/update/:id',uploadEvent.single('image'), (req, res) => {
  eventsController.update(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});

// ***************** Api delete for user *****************
router.delete('/delete/:id', (req, res) => {
  eventsController.delete(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});


// ***************** Api get   by id  *****************
router.get('/get/:id', (req, res) => {
  eventsController.get(req.params.id).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});


// ***************** Api list users *****************
router.get('/list', (req, res) => {
  eventsController.getList().then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});

// ***************** Api meteo *****************
router.get('/checkmeteo', (req, res) => {
  let city = req.query.city;
  var request = require('request');
  request(`https://api.openweathermap.org/data/2.5/weather?q=${city},cnt=10,uk&callback=test&appid=e37f8b33f127449b032bb8b43cd992d2&mode=JSON`,  (error, response, body) =>{
        
          let data = response;
          console.log(typeof(response))
          try{       
            // var string1 = JSON.stringify(data.body);
            let body = data.body;
            let newArray = body.split("({");
            let str2 = newArray[1].substring(0, newArray[1].length - 1);
            str2 = "{"+str2;
            var parsed = JSON.parse(str2);
            console.log(parsed);
            res.data=parsed.weather[0].description;
            res.send(parsed.weather[0]);
          }
          catch(err){
            console.log(err)
          }

        
  });
});
// ***************** route participer  *****************
router.put('/participer/:iduser/:idevent', (req, res) => {
  eventsController.participer(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
router.put('/cparticiper/:iduser/:idevent', (req, res) => {
  eventsController.cancelparticiper(req).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// ***************** Filter *****************
router.get('/eventbycat/:cat', (req, res) => {
  eventsController.eventbyCategorie(req.params.cat).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
router.get('/supprix/:p', (req, res) => {
  eventsController.supprix(req.params.p).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
router.get('/minprix/:p', (req, res) => {
  eventsController.minprix(req.params.p).then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});
// ***************** user event participation *****************
router.get('/myevent', (req, res) => {
  eventsController.myevent().then((data) => {
    res.json(data);
  }).catch((err) => {
    res.status(400).json(err);
  });
});


module.exports = router;
