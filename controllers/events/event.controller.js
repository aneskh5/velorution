const req = require('express/lib/request');
const { status } = require('express/lib/response')
const eventModel = require('../../models/events/event.model')

// ***************** function add event *****************
module.exports.add = (req) => {  
    return new Promise((resolve, reject) => {
        
        let body = req.body ;
        if(body){
            eventDB = new eventModel(body);
            eventDB.image = req.file ? req.file.path : '';
            eventDB.save((err,data)=>{
                if(!err){
                    resolve({
                        response: true,
                        message: 'Event added successfully',
                        data: data
                    });
                }else{
                    resolve({
                        response: false,
                        message: 'You have missed data',
                        data: err
                    });
                }
            })
        }else{
            reject();
        }
    });
} 

// ***************** function update event *****************
module.exports.update = (req) => {
    return new Promise((resolve, reject) => {
        let body = req.body;
        body.image = req.file && req.file.path;
        if(body){
            eventModel.findOneAndUpdate({_id:req.params.id}, body)
            .then((res) =>{
                resolve({
                    response: true,
                    message: 'Event modify successfully',
                    data: res
                });
            }).catch(err =>{
                reject(err)
            });
        }else{
            reject();
        }
    });
} 

// ***************** function delete event *****************
module.exports.delete = (req) => {
    return new Promise((resolve,reject) => {

        eventModel.findOneAndDelete({_id:req.params.id}, function (err, data) {
            if (err){
                reject({
                    response: false,
                    message: 'error',
                    data: err
                });
            }
            else{
                resolve({
                    response: data != null ,
                    message: data != null ? 'Deleted event' : 'event doesnt exist',
                    data: data
                });
            }
        });
    })
} 

// ***************** Function get   by id  *****************
module.exports.get = (id) => {
    return new Promise((resolve,reject) => {
        eventModel.findOne({_id:id})
        .then((event)=>{
            condition = false;
            if(event)
                condition = true;
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: event
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 

// ***************** function list  *****************
module.exports.getList = () => {  
    return new Promise(async (resolve, reject) => {

        let events = await eventModel.find({});
        let condition = events.length > 0;
        if(condition)  {
            events.forEach((e,index) => {
                    e.image ="http://localhost:3000/"+e.image;
                    
                    if(events.length == index+1){
                        resolve({
                            response: condition,
                            message: condition ? 'Get data successfully' : 'No data',
                            data: events 
                        })

                    }
              });

        }
         
    });


} 
// ***************** function participer  *****************
// module.exports.participer = async (req,res) => {

//         let body = req.body;
//         if(body){
//             try{
//             var e= await eventModel.findOne({_id:req.params.idevent});
//             if (e.nbrparticipant<e.nbrparticipantmax){
//                 e.userparticipants.push(req.params.iduser);
//                 e.nbrparticipant++;
//                 e.save();
//               res.send('inscri')
//             }
//             else{
//                 console.log('events full ')
//             }
//         }
//         catch(err){console}
//         }
// } 
module.exports.participer = async (req,res) => {

        let body = req.body;
        if(body){
            try{
            var e= await eventModel.findOne({_id:req.params.idevent});
            if (e.nbrparticipant<e.nbrparticipantmax){
                e.userparticipants.push(req.params.iduser);
                e.nbrparticipant++;
                res.send(e)
              }
            else{
                console.log('events full ')
            }

        }
        catch(err){console.log(err)}
        }

} 
module.exports.cancelparticiper = async (req,res) => {

    let body = req.body;
    if(body){
        try{
        var e= await eventModel.findOne({_id:req.params.idevent});
            var vared = e.userparticipants.indexOf(req.params.iduser)
            e.userparticipants.splice(vared, 1);
            e.nbrparticipant--;
            e.save();
            console.log('inscri annulé avec success  ')
            res.send(e)
            
        } 

        catch (err){
            console.log(err)
        }
        
    }
} 
// ***************** Filter *****************
module.exports.eventbyCategorie = (cat) => {
    return new Promise((resolve,reject) => {
        eventModel.find({categorie:cat})
        .then((event)=>{
            condition = false;
            if(event)
                condition = true;
                console.log(event)
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: event
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 
module.exports.supprix = (p) => {
    return new Promise((resolve,reject) => {
        eventModel.find({prix: { $lt: p }})
        .then((event)=>{
            condition = false;
            if(event)
                condition = true;
                console.log(event)
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: event
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 
module.exports.minprix = (p) => {
    return new Promise((resolve,reject) => {
        eventModel.find({prix: { $gt: p }})
        .then((event)=>{
            condition = false;
            if(event)
                condition = true;
                console.log(event)
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: event
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 
module.exports.myevent = async (req,res) => {

        return new Promise((resolve,reject) => {
            eventModel.find()
            .then((e)=>{
                condition = false;
                if(e){
                    condition = true;
                    var myevents = [];
                e.forEach(function(event){
                event.userparticipants.forEach(function(iduser){
                    if (iduser=='62556325r'){
                        myevents.push(event);}
                });
                });
                myevents.forEach((e,index) => {
                e.image ="http://localhost:3000/"+e.image;
                });
                }
                resolve({
                    response: condition,
                    message: condition ? 'Get data successfully' : 'No data',
                    data: myevents
                });}) 
                .catch((err)=>{
                reject({
                    response: false,
                    message: 'No ID provided',
                    data: err
                })
            })
        }) 
} 
