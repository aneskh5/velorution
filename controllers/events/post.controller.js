const req = require('express/lib/request');
const postModel = require('../../models/events/post.model')
const eventModel = require('../../models/events/event.model')


// ***************** function add post *****************
module.exports.add = (req) => {  
    return new Promise((resolve, reject) => {
        
        let body = req.body ;
        if(body){
            postDB = new postModel(body);
            postDB.id_event=req.params.id;
            postDB.image = req.file ? req.file.path : '';
            postDB.save((err,data)=>{
                if(!err){
                    resolve({
                        response: true,
                        message: 'Post added successfully',
                        data: data
                    });
                }else{
                    resolve({
                        response: false,
                        message: 'You have missed data',
                        data: err
                    });
                }
            })
        }else{
            reject();
        }
    });
} 

// ***************** function update post *****************
module.exports.update = (req) => {
    return new Promise((resolve, reject) => {
        let body = req.body;
        body.image = req.file && req.file.path;
        if(body){
            postModel.findOneAndUpdate({_id:req.params.id}, body)
            .then((res) =>{
                resolve({
                    response: true,
                    message: 'User modify successfully',
                    data: res
                });
            }).catch(err =>{
                reject(err)
            });
        }else{
            reject();
        }
    });
} 

// ***************** function delete post *****************
module.exports.delete = (req) => {
    return new Promise((resolve,reject) => {

        postModel.findOneAndDelete({_id:req.params.id}, function (err, data) {
            if (err){
                reject({
                    response: false,
                    message: 'error',
                    data: err
                });
            }
            else{
                resolve({
                    response: data != null ,
                    message: data != null ? 'Deleted post' : 'post doesnt exist',
                    data: data
                });
            }
        });
    })
} 

// ***************** Function get   by id  *****************
module.exports.get = (id) => {
    return new Promise((resolve,reject) => {
        postModel.findOne({_id:id}).then((post)=>{
            condition = false;
            if(post)
                condition = true;
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: post
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 

// ***************** function list  *****************
module.exports.getList = () => {  
    return new Promise(async (resolve, reject) => {

        let posts = await postModel.find({});
        let condition = posts.length > 0;
        resolve({
            response: condition,
            message: condition ? 'Get data successfully' : 'No data',
            data: posts
        })
    });
} 
// ***************** j'aime action  *****************
module.exports.jaime = async (req,res) => {

    try{
        var p= await postModel.findOne({_id:req.params.idpost});
            if (!(p.jaimeplus.find(element => element = req.params.iduser)||(p.jaime.find(element => element = req.params.iduser)) )){
                p.jaime.push(req.params.iduser);
                p.save();
                console.log("j'aime plus ");
                console.log(p);
            }
        
            else
            {   let i=p.jaime.findIndex(element => element = req.params.iduser)
                console.log(i)
                p.jaime.splice( i,1);
                p.save();
                console.log("v");
            }
        }
    catch(err)
            {
                console.log(err)
            }

  
} 
// ***************** j'aime plus action  *****************
module.exports.jaimeplus = async (req,res) => {

    try{
        var p= await postModel.findOne({_id:req.params.idpost});
            if (!(p.jaimeplus.find(element => element = req.params.iduser)||(p.jaime.find(element => element = req.params.iduser)) )){
                p.jaimeplus.push(req.params.iduser);
                p.save();
                console.log("j'aime plus ");
                console.log(p);
            }
            else
            {   let i=p.jaimeplus.findIndex(element => element = req.params.iduser)
                console.log(i)
                p.jaimeplus.splice( i,1);
                p.save();

                console.log("vous avez deja fait un dislike ");
            }

    }
    catch(err){
        console.log(err)
    }
    
} 
// event posts
module.exports.geteventposts = (id) => {
    return new Promise((resolve,reject) => {
        postModel.find({id_event:id}).then((posts)=>{
            condition = false;
            if(posts)
                condition = true;
            resolve({
                response: condition,
                message: condition ? 'Get data successfully' : 'No data',
                data: posts
            });
        }).catch((err)=>{
            reject({
                response: false,
                message: 'No ID provided',
                data: err
            })
        })
    })
} 





