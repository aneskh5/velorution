const mongoose = require('mongoose');
const { Schema } = mongoose;

const PostSchema = new Schema({
  content:  { type: String, default: "" },
  image:  { type: String, default: "" },
  jaime: [[String]],
  jaimeplus: [[String]],
  datepost:  { type: Date, default: Date.now },
  id_event:{type: String, default: "" } ,
  id_user: {type: String, default: ""}  ,
},{timestamps: true});

let Post = mongoose.model('Post',PostSchema);

module.exports = Post;
